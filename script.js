'use strict'
const ipBtn = document.querySelector('.ip-btn');
const infoBlock = document.querySelector('.info-block');

ipBtn.addEventListener('click', async () => {
    infoBlock.innerHTML = '';
    await makeRequest();
})

async function makeRequest() {
    try {
        let response = await fetch('http://ip-api.com/json/?fields=continent,country,regionName,city,district,query')
        let infoObj = await response.json();
        renderIpInfo(infoObj);
    } catch (error) {
        console.log(error.message);
    }
}

function renderIpInfo({ city, country, regionName, continent, district, query }) {
    infoBlock.innerHTML = `
        <p><b>Continent:</b> ${continent}</p>
        <p><b>Country:</b> ${country}</p>
        <p><b>Region name:</b> ${regionName}</p>
        <p><b>City:</b> ${city}</p>
        <p><b>District:</b> ${district}</p>
        <p><b>IP:</b> ${query}</p>`
}